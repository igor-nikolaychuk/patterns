#include <iostream>
#include <memory>
#include <vector>

class TextProcessor;
class PrintingStrategy;

using PrintingStrategyPtr = std::unique_ptr<PrintingStrategy>;
using PrintableArray = std::vector<int>;

//Abstact printer
class PrintingStrategy
{
public:
    //Default array printing logic
    virtual void printArray(const PrintableArray& a) {
        std::cout << "Printing strategy not set" << std::endl;
    }
};

//A machine from the description
class TextProcessor
{
    //Pointer to current printing strategy
    PrintingStrategyPtr printer;
public:
    TextProcessor(): printer(std::make_unique<PrintingStrategy>()) { }

    void printArray(PrintableArray& a) {
        printer->printArray(a);
    }

    void setPriningStrategy(PrintingStrategyPtr p) {
        printer = std::move(p);
    }
};

// Implementing the printing strategy with text printing logic
class TextPrintingStrategy: public PrintingStrategy {
public:
    void printArray(const PrintableArray& a) override {
        for(int val: a) {
            std::cout << val << " ";
        }
        std::cout << std::endl;
    }
};

// Implementing the printing strategy with html printing logic
class HtmlPrintingStrategy: public PrintingStrategy {
public:
    void printArray(const PrintableArray& a) override {
        std::cout << "<ul>" << std::endl;

        for(int val: a) {
            std::cout << "\t<li>" << val << "</li> " << std::endl;
        }
        std::cout << "</ul>" << std::endl;
    }
};

int main() {
    PrintableArray testArray{4,2};

    // Create test instance of the text processor
    TextProcessor processor;

    // Prining when a concrete strategy not sot
    processor.printArray(testArray);

    processor.setPriningStrategy(std::make_unique<TextPrintingStrategy>());

    // Prining the array with text strategy
    processor.printArray(testArray)

    ;
    processor.setPriningStrategy(std::make_unique<HtmlPrintingStrategy>());

    // Prining the array with text prining strategy
    processor.printArray(testArray);
}