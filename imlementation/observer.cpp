#include <iostream>
#include <boost/any.hpp>
#include <vector>


using namespace std;
using boost::any;
using boost::any_cast;

struct Person;


/* Represents a person observer interface
 * containing notification method */
struct PersonListener {
    virtual ~PersonListener() = default;
    //Method to be called on every state change
    virtual void personChanged(Person& p, //Object reference
    const string& propertyName,           //Name of changed property
    const any newValue) = 0;              //New property value
};

//Observation subject
class Person {
    //Observable property
    int age;
    //Set of change observers/listeners
    vector<shared_ptr<PersonListener>> listeners;

    /* Inmplementation of notification logic
     * Receive property name and it's new value */
    void notify(const string& propertyName, const any newValue) {
        //Iterate through the listeners list
        for(const auto l: listeners) {
            /* And call the notification  method passing
             * a refernce to the current Person object, property name
             * and updated value */
            l->personChanged(*this, propertyName, newValue);
        }
    }
public:
    Person(int age) : age(age) {}

    int getAge() const {
        return age;
    }

    //Age property setter
    void setAge(int age) {
        Person::age = age;  //Change actual property
        notify("age", age); //Notify the listeners about the change
    }

    //Observable subscription method accepting a listener
    void subscribe(shared_ptr<PersonListener> listener) {
        //Just add the listener to the vector
        listeners.push_back(move(listener));
    }
};

/* Actual implementation of abstract person observer implementing
 * the virtual notification handler method "personChanged" */
struct ConsoleListener: public  PersonListener {
    void personChanged(Person &p, const string &propertyName, const any newValue) override {
        // Just print the updated property and it's value
        cout << "Person's " << propertyName << " has been changed to ";
        if(propertyName == "age")
            cout << any_cast<int>(newValue);

        cout << endl;
    }
};

int main() {
    // Create a test observation subject
    Person person {20};

    // Create a test observer
    person.subscribe(make_shared<ConsoleListener>());

    // Update person age expecting notification of the observer
    person.setAge(21);

    // Add an extra observer
    person.subscribe(make_shared<ConsoleListener>());

    // Update person age expecting notification of two observers
    person.setAge(22);

    return 0;
}