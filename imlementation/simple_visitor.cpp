/* Let's suppose we need a component which prints the information about
 * a set of computer components. So that the logic of information gathering
 * will implemented outside component classes */

#include <iostream>
#include <memory>

class CPU;
class RAM;

//Implementing printing logic in a separate class
class PCComponentInfoPrinter {
public:
    void visit(CPU& o);
    void visit(RAM& o);
};

// Defining a component interface representing a computer component
struct IPCComponent {
    /* Defining a function allowing the printer to get a reference to
     * actual object */
    virtual void accept(PCComponentInfoPrinter& visitor) = 0;
};


//Defining PC components with different properties
struct CPU: public IPCComponent {
    std::string manufacturer;
    std::size_t frequency;

    void accept(PCComponentInfoPrinter& visitor) override {
        visitor.visit(*this);
    }
};

struct RAM: public IPCComponent {
    std::string manufacturer;
    std::size_t frequency;
    std::size_t size;

    void accept(PCComponentInfoPrinter& visitor) override {
        visitor.visit(*this);
    }
};

struct RAM2 {
    std::string manufacturer;
    std::size_t frequency;
    std::size_t size;
};

//Providing actual implementation of printer methods
void PCComponentInfoPrinter::visit(CPU &o) {
    std::cout << "[CPU] Manufacturer: " << o.manufacturer
              << " frequency " << o.frequency << std::endl;
}

void PCComponentInfoPrinter::visit(RAM &o) {
    std::cout << "[RAM] Manufacturer: " << o.manufacturer
              << " frequency " << o.frequency
              << " size " << o.size << std::endl;
}

int main() {
    PCComponentInfoPrinter printer;

    //Initializing objects
    CPU cpu;
    cpu.manufacturer = "Intel";
    cpu.frequency = 42;

    RAM ram;
    ram.manufacturer = "Kingston";
    ram.frequency = 2048;
    ram.size = 4096;

    //Priting their info
    cpu.accept(printer);
    ram.accept(printer);
}