#include <iostream>
#include <memory>

using namespace std;

/* Let's suppose we have a complex operation
 * consisting of a few phases.
 * Let's define the operation skeleton and it's execution logic. */
class ComplexOperationSkeleton
{
protected:
    virtual void phase1() = 0;
    virtual void phase2() = 0;

public:
    void execute() {
        phase1();
        phase2();
    }
};

//Let's define the actual implementation of the phases
class ComplexOperation: public ComplexOperationSkeleton
{
protected:
    virtual void phase1() override {
        std::cout << "Very complex phase 1..." << std::endl;
    }

    virtual void phase2() override {
        std::cout << "Very complex phase 2..." << std::endl;
    }
};

int main() {
    //Test the operation
    ComplexOperation().execute();
}