#include <iostream>
#include <memory>

using namespace std;

class Machine;
class State;

using StatePtr = std::unique_ptr<State>;

//Machine state interface
class State
{
public:
    /* Default method handlers which are called when
     * the current state don't allow the operation */

    virtual void on(Machine& m) {
        std::cout << "Wrong state! Machine already off" << std::endl;
    }

    virtual void off(Machine& m) {
        std::cout << "Wrong state! Machine already on" << std::endl;
    }

    //Returns OffState by default
    static StatePtr getDefault();
};

//A machine from the description
class Machine
{
    //Pointer to current state
    StatePtr current;
public:
    Machine(): current(State::getDefault()) { }

    void updateState(StatePtr s) {
        current = std::move(s);
    }

    /* Just call appropriate methods on current state objects,
     * if the state is allowed, actual handler will be called,
     * otherwise - default error will be used*/

    void on() {
        current->on(*this);
    }

    void off() {
        current->off(*this);
    }
};

/* Actual state implementations. Only allowed methods are
 * required to be implemented */
class OnState: public State
{
public:
    void off(Machine& m);
};

class OffState: public State
{
public:
    void on(Machine& m);
};

// On/off logic and state switching
void OffState::on(Machine &m) {
    std::cout << "Turning on machine" << std::endl;

    m.updateState(std::make_unique<OnState>());
}

void OnState::off(Machine &m) {
    std::cout << "Shutting down the machine" << std::endl;

    m.updateState(std::make_unique<OffState>());
}


StatePtr State::getDefault() {
    return std::make_unique<OffState>();
}

int main() {
    //Create a test machine instance
    Machine machine;

    //Turning the machine on. Expected to be done successfully
    machine.on();

    //Turning the machine on again. Fail expected
    machine.on();

    //Turning the machine off. Expected to be done successfully
    machine.off();

    //Turning the machine off again. Fail expected
    machine.off();
}